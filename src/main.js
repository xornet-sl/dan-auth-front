// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

import ru from 'vee-validate/dist/locale/ru'
import VeeValidate, { Validator } from 'vee-validate'

Validator.localize('ru', ru)
Vue.use(VeeValidate)

Vue.use(Vuetify)

window.serverUrl =  process.env.HOST_BASE_URL

window.axios = require('axios')
window.axios.defaults.baseURL = process.env.HOST_BASE_URL
window.axios.defaults.withCredentials = (process.env.NODE_ENV==="production")
window.axios.defaults.headers.common = {
  'X-Requested-With': 'XMLHttpRequest',
}

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
