eq = $(and $(findstring x$(1),x$(2)), $(findstring x$(2),x$(1)))

APP=auth
PROJECT=gitlab.com/dantser/frontend/auth
REGISTRY?=registry.gitlab.com/founder-sl
#REGISTRY?=registry.gitlab.com/dantser/frontend

RELEASE?=0.0.7

HOST_BASE_URL?=https://dantser.net/api/accounts/v1

NAMESPACE?=prod

# Infrastructure: dev, stable, test ...
INFRASTRUCTURE?=stable
VALUES?=values-${INFRASTRUCTURE}

#CONTAINER_IMAGE?=${REGISTRY}/${APP}
CONTAINER_IMAGE?=${REGISTRY}/dan-auth-front
CONTAINER_TAG?=$(if $(or $(call eq,${NAMESPACE},prod),$(call eq,${NAMESPACE},staging)),${RELEASE},review-${NAMESPACE})
CONTAINER_NAME?=${APP}-frontend-${NAMESPACE}

REPO_INFO=$(shell git config --get remote.origin.url)

ifndef CI_COMMIT_SHA
CI_COMMIT_SHA := git-$(shell git rev-parse HEAD)
endif

BUILDTAGS=

.PHONY: vendor
vendor:
	npm install

.PHONY: build
build: vendor
	@echo "+ $@"
	@npm run build

.PHONY: build-image
build-image:
	docker build --pull -t $(CONTAINER_IMAGE):$(CONTAINER_TAG) .

.PHONY: push
push: build-image
	@echo "+ $@"
	@docker push $(CONTAINER_IMAGE):$(CONTAINER_TAG)

.PHONY: run
run: build
	@echo "+ $@"
	@docker run --name ${CONTAINER_NAME} -p 8080:80 \
		-e "HOST_BASE_URL=$(HOST_BASE_URL)" \
		-d $(CONTAINER_IMAGE):$(CONTAINER_TAG)
	@sleep 1
	@docker logs ${CONTAINER_NAME}

.PHONY: deploy
deploy: push
	helm upgrade ${CONTAINER_NAME} -f charts/${VALUES}.yaml charts --namespace ${NAMESPACE} --version=${RELEASE} -i --wait
